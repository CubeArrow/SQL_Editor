import java.util.ArrayList;

public class ValueSaver {
    public static String tableName;
    public static  String[] columnNames;
    public static ArrayList<String[]> rows;

    public ValueSaver(String tableName, String[] columnNames, ArrayList<String[]> rows){
        this.tableName = tableName;
        this.columnNames = columnNames;
        this.rows = rows;
    }
}
